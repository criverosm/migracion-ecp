CNS_DEUDA = "call SP_STOCK_DEUDA(%s);"
CNS_CAB = "call SP_ENCABEZADO_ECP(%s);"
TRC_TBL="delete from (%s);"


CNS_MATRIZ_GARANTIAS = "call SP_RESUMEN_MATRIZ_GARANTIAS();"
CNS_FBL5N ="call SP_DEUDA_FBL5N_30D(%s);"
CNS_SIISA ="call SP_RIESGO_MERCADO_SIISA(%s);"
CNS_GARANTIAS ="call SP_GARANTIAS_ENTERADAS(%s);"
CNS_DECISION = "call SP_VARIABLES_DECISION(%s);"
CNS_GARANTIAS_INV = "call SP_GARANTIAS_PENDIENTES_INVENTARIO(%s);"


INS_DATOS = "CALL SP_LIMPIA_DATOS_STG(%s);"
INS_CARGA = "CALL SP_CARGA_INPUT_DATOS(%s);"

CNS_DEUDA30D = "call SP_DEUDA_FBL5N_30D(%s);"


INS_UF = \
    """
     LOAD DATA LOCAL INFILE '/tmp/TBL_UF_SQL.csv' INTO TABLE STG_TBL_PRECIO_UF
    FIELDS TERMINATED BY ';' 
    ENCLOSED BY '"' 
    LINES TERMINATED BY '\r\n'
    IGNORE 1 LINES
    (Ano,Mes,Dia,Fecha,Precio,Unidad);
    """
  
INS_BUC_BERTIN = \
    """
    LOAD DATA LOCAL INFILE '/tmp/TBL_BUC_BERTIN_SQL.csv' INTO TABLE STG_TBL_BUC_BERTIN
    FIELDS TERMINATED BY ';' 
    ENCLOSED BY '"' 
    LINES TERMINATED BY '\r\n'
    IGNORE 1 LINES
    (Rut_Cuenta_Deudor, DV, Q, DVR, Razon_Social, Fantasia, Holding_Grupo, SubGte_COM, Rubro_CC, Rubro_CC_Especifico, Ejecutivo_CC, NdC, Enviar_Fisico_IBR, Tipo_Cliente_IBR, Contacto_Receptor_Factura_PDF_Paperless_DEFINITIVO, Propuesta_contacto_receptor_factura_pdf_paperless, Propuesta_correo_contacto_IBR, Propuesta_telefono_Contacto_IBR, Forma_de_Pago_IBR, Modalidad_de_Pago_IBR, Nombre_Completo_1, Cargo_1, Especialidad_Rubro_que_Paga, SUC_CTTO, Email_1, Fono_1, Nombre_Completo2, Cargo_2, Email_2, Fono_2, Direccion_de_despacho, Nombre_3, Email_3, Fono_3, Fono_4, Email_4, Email_5, Email_6, Email_7, Fecha_Actualizacion);
    """
    
INS_RELATORIOS = \
    """
    LOAD DATA LOCAL INFILE '/tmp/TBL_RELATORIOS_BW.csv' INTO TABLE STG_RELATORIOS_BW
    FIELDS TERMINATED BY ';' 
    ENCLOSED BY '"' 
    LINES TERMINATED BY '\r\n'
    IGNORE 1 LINES
    (Cliente, nombre, Ano_mes_natural, ano_mes, Venta);
    """
INS_STG_BW = \
    """
    LOAD DATA LOCAL INFILE '/tmp/TBL_BW.csv' INTO TABLE STG_TBL_BW
    FIELDS TERMINATED BY ';' 
    ENCLOSED BY '"' 
    LINES TERMINATED BY '\r\n'
    IGNORE 1 LINES
    (Sociedad, Clave_referencia1, Cliente, Razon_social_tmp, Vencimiento_neto, Contrato, nombre_fantasia_tmp, Indicador_CME, Descripcion_Indicador_CME_tmp, Clase_documento, descripcion_clase_documento_tmp, Importe);
    """
INS_FBL1N = \
    """
    LOAD DATA LOCAL INFILE '/tmp/TBL_FBL1N_SQL.csv' INTO TABLE STG_TBL_FBL1N
    FIELDS TERMINATED BY ';' 
    ENCLOSED BY '"' 
    LINES TERMINATED BY '\r\n'
    IGNORE 1 LINES
    (Sociedad, Cuenta, Numero_contrato, Centro_beneficio, N_documento, Asignacion, Indicador_CME, Clase_documento, Referencia, Fecha_entrada, Fecha_documento, Condiciones_pago, Fecha_pago, Demora_en_DPP1, Moneda_local, Importe_moneda_local, Moneda_documento, Importe_moneda_doc, Clave_referencia1, Texto, Ejercicio, Fecha_Actualizacion);
    """
    
INS_FBL5N = \
    """
    LOAD DATA LOCAL INFILE '/tmp/TBL_FBL5N_SQL.csv' INTO TABLE STG_TBL_FBL5N
    FIELDS TERMINATED BY ';' 
    ENCLOSED BY '"' 
    LINES TERMINATED BY '\r\n'
    IGNORE 1 LINES
    (Icono_partabiertas_comp, Sociedad, Segmento, Centro_de_beneficio, Cuenta, Cuenta_de_mayor, Indicador_CME, Clase_de_documento, Referencia, Asignacion, Clave_referencia1, Fe_contabilizacion, Fecha_de_pago, Demora_tras_vencimiento_neto, Importe_en_moneda_local, Numero_de_contrato, Texto, Fecha_compensacion, Doc_compensacion, id);
    """
INS_SIISA = \
    """
    LOAD DATA LOCAL INFILE '/tmp/TBL_SIISA_SQL.csv' INTO TABLE STG_TBL_SIISA
    FIELDS TERMINATED BY ';' 
    ENCLOSED BY '"' 
    LINES TERMINATED BY '\r\n'
    IGNORE 1 LINES
    (RUT, DV, NOMBRE, Par_Ordenado, HT_CH, P_CH, HT_LT, P_LT, HT_PG, P_PG, HT_CM, P_CM, HT_MIC, P_MIC, HT_IF, P_IF, HT_M, P_M, RANKING, `GENERAL`, Fecha_Actualizacion);
    """
INS_ZCLRE46 = \
    """
    LOAD DATA LOCAL INFILE '/tmp/TBL_ZCLRE46_SQL.csv' INTO TABLE STG_TBL_ZCLRE46
    FIELDS TERMINATED BY ';' 
    ENCLOSED BY '"' 
    LINES TERMINATED BY '\r\n'
    IGNORE 1 LINES
    (Sociedad, UE, Nro_Contrato, Fantasia, SUC, Denominacion_gpo_autorizacion, Denominacion, Fecha_Inicio, Fecha_Termino, InicContr, Fin_Contrato, Rescis_Contrato, ReVN, Interl_comercial, Nombre_Completo, Inic_rel, Final_rel, Base, Un, Base1, Un_1, ST_CTR, ST_OBJ);
    """
INS_ZCLRE47 = \
    """
    LOAD DATA LOCAL INFILE '/tmp/TBL_ZCLRE47_SQL.csv' INTO TABLE STG_TBL_ZCLRE47
    FIELDS TERMINATED BY ';' 
    ENCLOSED BY '"' 
    LINES TERMINATED BY '\r\n'
    IGNORE 1 LINES
    (sociedad, numero_contrato, denominacion_contrato, InicContr, Fin_vigen, PrimFinCon, Rescis_el, Interl_comercial, Final_rel, TpMed, Tipo_medida, Medida_de, Medida_a, Cantidad, unidad, status, Vigente);
    """
INS_ZCLRE53 = \
    """
    LOAD DATA LOCAL INFILE '/tmp/TBL_ZCLRE53_SQL.csv' INTO TABLE STG_TBL_ZCLRE53
    FIELDS TERMINATED BY ';' 
    ENCLOSED BY '"' 
    LINES TERMINATED BY '\r\n'
    IGNORE 1 LINES
    (cliente, rut, nombre, direccion, distrito, poblacion, giro_comercial, telefono, fax, email, f_creacion, fecha_archivo);
    """
INS_REISCN = \
    """
    LOAD DATA LOCAL INFILE '/tmp/TBL_REISCN.csv' INTO TABLE STG_TBL_REISCN
    FIELDS TERMINATED BY ';' 
    ENCLOSED BY '"' 
    LINES TERMINATED BY '\r\n'
    IGNORE 1 LINES
    (Sociedad, Clase_contrato, Contrato, Den_cl_contrato, Denominacion_contrato, Linea_valida_de, Primer_fin_contrato, contabil_desde, Fe_in_flj_financiero, UnidadEconomicaContr, Denominacion_gpo_autorizacion, Sector_industrial, Denominacion_ramo, Inventario_economico, Status_actual, Status_actual2, Rescision_el, Per_vigencia_meses, Linea_valida_hasta, Autor);
    """
    
    
