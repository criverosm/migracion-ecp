from Dao import sql
import pymysql as pymysql
import os
import logging
pathlog = '/tmp/ecp.log'



host = os.environ.get('HOST_BD')
db = os.environ.get('DB')
user= os.environ.get('USER_DB')
password=os.environ.get('DB_PASS')


#host = "45.79.197.178"
#db = "ecp"
#user = "admin"
#password = "admin"


logging.basicConfig(format='%(asctime)s %(message)s', datefmt='%Y/%m/%d %I:%M:%S %p', filename=pathlog, level=logging.INFO)




def likeOperator(word):
    return "%" + word + "%"


def connection():
    return pymysql.connect(
        host=host,
        db=db,
        user=user,
        password=password,
        local_infile=True
    )
        
def cnsCabecera(rut):
    conn = connection()
    try:
        with conn.cursor() as cur:
            cur.execute(sql.CNS_CAB,(rut,))
            cab=cur.fetchall();
            return cab
    finally:
        conn.close()
            
def cnsDeuda(rut):
    conn = connection()
    try:
        with conn.cursor() as cur:
            cur.execute(sql.CNS_DEUDA,(rut,))
            deuda=cur.fetchall();
            return deuda
    finally:
        conn.close()

def cnsDeuda30D(rut):
    conn = connection()
    try:
        with conn.cursor() as cur:
            cur.execute(sql.CNS_DEUDA30D,(rut,))
            deuda=cur.fetchall();
            return deuda
    finally:
        conn.close()
        
def cnsfbl5n(rut):
    conn = connection()
    try:
        with conn.cursor() as cur:
            cur.execute(sql.CNS_FBL5N,(rut,))
            deuda=cur.fetchall();
            return deuda
    finally:
        conn.close()
        
        
def cnsSiisa(rut):
    conn = connection()
    try:
        with conn.cursor() as cur:
            cur.execute(sql.CNS_SIISA,(rut,))
            siisa=cur.fetchall();
            return siisa
    finally:
        conn.close()
        
        
def cnsGarantiasEnt(rut):
    conn = connection()
    try:
        with conn.cursor() as cur:
            cur.execute(sql.CNS_GARANTIAS,(rut,))
            deuda=cur.fetchall();
            return deuda
    finally:
        conn.close()
        
def cnsDecision(rut):
    conn = connection()
    try:
        with conn.cursor() as cur:
            cur.execute(sql.CNS_DECISION,(rut,))
            deuda=cur.fetchall();
            return deuda
    finally:
        conn.close()
        
def cnsGarantiasInv(rut):
    conn = connection()
    try:
        with conn.cursor() as cur:
            cur.execute(sql.CNS_GARANTIAS_INV,(rut,))
            deuda=cur.fetchall();
            return deuda
    finally:
        conn.close()
        
def cnsMatrizGarantia():
    conn = connection()
    try:
        with conn.cursor() as cur:
            cur.execute(sql.CNS_MATRIZ_GARANTIAS)
            deuda=cur.fetchall();
            return deuda
    finally:
        conn.close()
        
def InsUf():
    conn = connection()
    try:
        with conn.cursor() as cur:
            cur.execute("TRUNCATE TABLE STG_TBL_PRECIO_UF;")
            conn.commit()
            cur.execute(sql.INS_UF)
            conn.commit()
            cur.execute(sql.INS_DATOS,("TBL_PRECIO_UF",))
            conn.commit()
            cur.execute(sql.INS_CARGA,("TBL_PRECIO_UF",))
            conn.commit()
            
    finally:
        conn.close()

def InsBucBertin():
    conn = connection()
    try:
        with conn.cursor() as cur:
            cur.execute("TRUNCATE TABLE STG_TBL_BUC_BERTIN;")
            conn.commit()
            cur.execute(sql.INS_BUC_BERTIN)
            conn.commit()
            cur.execute(sql.INS_DATOS,("TBL_BUC_BERTIN",))
            conn.commit()
            cur.execute(sql.INS_CARGA,("TBL_BUC_BERTIN",))
            conn.commit()
    finally:
        conn.close()
def InsRela():
    conn = connection()
    try:
        with conn.cursor() as cur:
            cur.execute("TRUNCATE TABLE STG_RELATORIOS_BW;")
            conn.commit()
            cur.execute(sql.INS_RELATORIOS)
            conn.commit()
            cur.execute(sql.INS_DATOS,("RELATORIOS_BW",))
            conn.commit()
            cur.execute(sql.INS_CARGA,("RELATORIOS_BW",))
            conn.commit()
    finally:
        conn.close()
        
def InsStgBw():
    conn = connection()
    try:
        with conn.cursor() as cur:
            cur.execute("TRUNCATE TABLE STG_TBL_BW;")
            conn.commit()
            cur.execute(sql.INS_STG_BW)
            conn.commit()
            cur.execute(sql.INS_DATOS,("TBL_BW",))
            conn.commit()
            cur.execute(sql.INS_CARGA,("TBL_BW",))
            conn.commit()
    finally:
        conn.close()
        
def InsFbl1n():
    conn = connection()
    try:
        with conn.cursor() as cur:
            cur.execute("TRUNCATE TABLE STG_TBL_FBL1N;")
            conn.commit()
            cur.execute(sql.INS_FBL1N)
            conn.commit()
            cur.execute(sql.INS_DATOS,("TBL_FBL1N",))
            conn.commit()
            cur.execute(sql.INS_CARGA,("TBL_FBL1N",))
            conn.commit()
    finally:
        conn.close()
    
def InsFbl5n():
    conn = connection()
    try:
        with conn.cursor() as cur:
            cur.execute("TRUNCATE TABLE STG_TBL_FBL5N;")
            conn.commit()
            cur.execute(sql.INS_FBL5N)
            conn.commit()
    finally:
        conn.close()
        
        
def InsFbl5nIns():
    conn = connection()
    try:
        with conn.cursor() as cur:
            cur.execute(sql.INS_DATOS,("TBL_FBL5N",))
            conn.commit()
    finally:
        conn.close()
        

def InsFbl5nIn():
    conn = connection()
    try:
        with conn.cursor() as cur:
       
            cur.execute(sql.INS_CARGA,("TBL_FBL5N",))
            conn.commit()
    finally:
        conn.close()
        
def InsSiisa():
    conn = connection()
    try:
        with conn.cursor() as cur:
            cur.execute("TRUNCATE TABLE STG_TBL_SIISA;")
            conn.commit()
            cur.execute(sql.INS_SIISA)
            conn.commit()
            cur.execute(sql.INS_DATOS,("TBL_SIISA",))
            conn.commit()
            cur.execute(sql.INS_CARGA,("TBL_SIISA",))
            conn.commit()
    finally:
        conn.close()
        
def InsZclre46():
    conn = connection()
    try:
        with conn.cursor() as cur:
            cur.execute("TRUNCATE TABLE STG_TBL_ZCLRE46;")
            conn.commit()
            cur.execute(sql.INS_ZCLRE46)
            conn.commit()
            cur.execute(sql.INS_DATOS,("TBL_ZCLRE46",))
            conn.commit()
            cur.execute(sql.INS_CARGA,("TBL_ZCLRE46",))
            conn.commit()
    finally:
        conn.close()
        
def InsZclre47():
    conn = connection()
    try:
        with conn.cursor() as cur:
            cur.execute("TRUNCATE TABLE STG_TBL_ZCLRE47;")
            conn.commit()
            cur.execute(sql.INS_ZCLRE47)
            conn.commit()
            cur.execute(sql.INS_DATOS,("TBL_ZCLRE47",))
            conn.commit()
            cur.execute(sql.INS_CARGA,("TBL_ZCLRE47",))
            conn.commit()
    finally:
        conn.close()
        
def InsZclre53():
    conn = connection()
    try:
        with conn.cursor() as cur:
            cur.execute("TRUNCATE TABLE STG_TBL_ZCLRE53;")
            conn.commit()
            cur.execute(sql.INS_ZCLRE53)
            conn.commit()
            cur.execute(sql.INS_DATOS,("TBL_ZCLRE53",))
            conn.commit()
            cur.execute(sql.INS_CARGA,("TBL_ZCLRE53",))
            conn.commit()
    finally:
        conn.close()

def InsReiscn():
    conn = connection()
    try:
        with conn.cursor() as cur:
            cur.execute("TRUNCATE TABLE STG_TBL_REISCN;")
            conn.commit()
            cur.execute(sql.INS_REISCN)
            conn.commit()
            #cur.execute(sql.INS_DATOS,("TBL_REISCN",))
            #conn.commit()
            #cur.execute(sql.INS_CARGA,("TBL_REISCN",))
            #conn.commit()
    finally:
        conn.close()







