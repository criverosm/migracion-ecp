import boto3
import logging
import os
from Models.Response import response
from Dao.connectionDBMSQ import InsUf,InsBucBertin,InsRela,InsStgBw,InsFbl1n,InsFbl5n,InsSiisa,InsZclre46,InsZclre47,InsZclre53,InsReiscn,InsFbl5nIns,InsFbl5nIn


pathlog = os.environ.get('PATHLOG')
bucket = os.environ.get('BUCKET')
aws_access_key_id = os.environ.get('AWS_ACCESS_KEY_ID')
aws_secret_access_key = os.environ.get('AWS_SECRET_ACCESS_KEY')
region_name = os.environ.get('REGION_NAME')
#pathlog = os.environ.get('PATHLOG')


#bucket = 'parauco-ecp'
#aws_access_key_id = 'AKIA2DZLELCQ6ZNHRXUJ'
#aws_secret_access_key = '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'
#region_name = 'us-east-1'
#pathlog = '/tmp/ecp.log'


session = boto3.Session(
        aws_access_key_id,
        aws_secret_access_key)
client = session.client('s3')

logging.basicConfig(format='%(asctime)s %(message)s', datefmt='%Y/%m/%d %I:%M:%S %p', filename=pathlog, level=logging.INFO)


def Insert(filename,tipo):
        inserta = False
        try:
    
            if filename=='TBL_UF_SQL.csv':
                InsUf()
                inserta = True

            if filename == 'TBL_BUC_BERTIN_SQL.csv':
                InsBucBertin()
                inserta = True
            
            if filename == 'TBL_RELATORIOS_BW.csv':
                InsRela()
                inserta = True

            if filename == 'TBL_BW.csv':
                InsStgBw()
                inserta = True
           
            if filename == 'TBL_FBL1N_SQL.csv':
                InsFbl1n()
                inserta = True
                
            if filename == 'TBL_FBL5N_SQL.csv':
                
                if (tipo=='stg'):
                    InsFbl5n() 
                    inserta = True   
                if (tipo=='clean'):
                    InsFbl5nIns()
                    inserta = True
                if (tipo=='carga'):
                    InsFbl5nIn()
                    inserta = True      

            if filename == 'TBL_SIISA_SQL.csv':
                InsSiisa()
                inserta = True
                
            if filename == 'TBL_ZCLRE46_SQL.csv':
                InsZclre46()
                inserta = True

            if filename == 'TBL_ZCLRE47_SQL.csv':
                InsZclre47()
                inserta = True

            if filename == 'TBL_ZCLRE53_SQL.csv':
                InsZclre53()
                inserta = True

            if filename == 'TBL_REISCN.csv':
                InsReiscn()
                inserta = True
                
        except Exception as e:
            logging.error('%s Error en la carga de : %s' , e, filename)
            return None
        
        return inserta

def FindFiles():
    try:
        list =[]
        for obj in client.list_objects_v2(Bucket=bucket, Prefix="input/")['Contents']:
            folder = obj['Key']
            filename = folder[folder.rfind('/') + 1:len(folder)]
            if len(filename) > 0:
                list.append(filename)
    except Exception as e:
        logging.error('%s Error en la ejecución: %s' , 'Ecp', e)
        return response('500','Error Interno del Servidor',str(e))        
    return response('200','Lista de Archivos en Bucket S3 ',list) 

def convert_bytes(num):

    for x in ['bytes', 'KB', 'MB', 'GB', 'TB']:
        if num < 1024.0:
            return "%3.1f %s" % (num, x)
        num /= 1024.0



def file_size(file_path):

    if os.path.isfile(file_path):
        file_info = os.stat(file_path)
        return convert_bytes(file_info.st_size)

def InsFiles(namefile,tipo):
    try:
        existe = False
        logging.basicConfig(format='%(asctime)s %(message)s', datefmt='%Y/%m/%d %I:%M:%S %p', filename=pathlog, level=logging.DEBUG)
        for obj in client.list_objects_v2(Bucket=bucket, Prefix="input/")['Contents']:
            folder = obj['Key']
            filename = folder[folder.rfind('/') + 1:len(folder)]
            if len(filename) > 0 and filename == namefile:
                existe= True
                logging.info('%s descargando Archivo : %s' , 'Ecp', filename)
                if (tipo == '' or tipo == 'stg'):
                    client.download_file(bucket, folder, '/tmp/'+filename)
                #file_size('/tmp/'+filename) 
                carga = Insert(filename,tipo)                            
    except Exception as e:
        logging.error('%s Error en la ejecución: %s' , 'ecp', e)
        return response('500','Error Interno del Servidor',str(e))        
    if (not existe):
        return response('404','Archivo no se encuentra en Bucket S3' ,namefile)
    elif(carga): 
            return response('200','Archivo Cargado Exitosamente',namefile)
    elif(not carga and carga is not None):
        return response('404','Archivo No Corresponde a Tabla , verifique nombre de archivo cargado en S3 ',namefile)
    elif(carga is None):
        return response('500','Error en la carga de Archivo',namefile)
    




        

  
        
