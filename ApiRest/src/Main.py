#!flask/bin/python
from flask import Flask, jsonify
from flask import request
from Process import  Consulta_Ecp, Cns_Garantia
from ProcessFiles import FindFiles, InsFiles
import simplejson as json
import logging
from Models.Response import response
import os

pathlog = os.environ.get('PATHLOG')
#pathlog = '/tmp/ecp.log'
logging.basicConfig(format='%(asctime)s %(message)s', datefmt='%Y/%m/%d %I:%M:%S %p', filename=pathlog, level=logging.INFO)

app = Flask(__name__)
app.config['JSON_SORT_KEYS'] = False
app.config['JSONIFY_PRETTYPRINT_REGULAR'] = True

@app.route('/ecp/consultaRiesgo', methods=['GET', 'POST'])
def get_Ecp():    
    
        if request.method == 'GET':
            rut = request.args.get('rut', '')
            if not rut :
                logging.error('%s no se encuentra parametro rut ' , 'Ecp')
                return jsonify(response('400','No se encuentra parametro obligatorio Rut',rut))          
            else:
                try: 
                    int(rut)
                    logging.info('%s Ingresando consulta GET con rut : %s' , 'Ecp', rut)
                    return jsonify(Consulta_Ecp(rut))
                except ValueError:
                    logging.error('%s no se encuentra parametro rut ' , 'Ecp')
                    return jsonify(response('400','Rut invalido, utilice solo campos numericos sin digito verificador',rut)) 
        if request.method == 'POST':
            rut = request.json.get('rut')   
            if not rut :
                logging.error('%s no se encuentra parametro rut ' , 'Ecp')
                return jsonify(response('400','No se encuentra parametro obligatorio Rut',rut))         
            else:
                try: 
                    int(rut)
                    logging.info('%s Ingresando consulta POST con rut : %s' , 'Ecp', rut)
                    return jsonify(Consulta_Ecp(rut))
                except ValueError:
                    logging.error('%s no se encuentra parametro rut ' , 'Ecp')
                    return jsonify(response('400','Rut invalido, utilice solo campos numericos sin digito verificador',rut)) 
                

@app.route('/ecp/garantias', methods=['GET', 'POST'])
def get_Garantia():
        logging.info('%s Ingresando consulta GET garantias :' , 'Garantias')
        return jsonify(Cns_Garantia())
                         
@app.route('/ecp/archivos', methods=['GET', 'POST'])
def get_Files():
        logging.info('%s Ingresando consulta GET archivos :' , 'Archivos')
        return jsonify(FindFiles())
    
@app.route('/ecp/cargaArchivo', methods=['POST'])    
def ins_File():
        if request.method == 'POST':
            filename = request.json.get('filename')
            if not filename :
                logging.error('%s no se encuentra parametro filename ' , 'Ecp')
                return jsonify(response('400','No se encuentra parametro obligatorio filename',filename))          
            else:
                try: 
                    if (filename == 'TBL_FBL5N_SQL.csv'):
                        tipo = request.json.get('tipocarga')   
                        logging.info('%s Ingresando consulta POST con filename : %s' , 'Ecp', filename)
                        return jsonify(InsFiles(filename,tipo))
                    else:
                        logging.info('%s Ingresando consulta POST con filename : %s' , 'Ecp', filename)
                        return jsonify(InsFiles(filename,''))
                except Exception as e:
                    logging.error('%s Error en la ejecucion %s ' , 'Archivos', e)
                    return jsonify(response('500','Error Interno del servidor',str(e)))
                                     
if __name__ == '__main__':
    app.run()
    
