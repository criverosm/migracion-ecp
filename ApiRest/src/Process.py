import datetime
from Models.Response import GarantiasEnterada,Ecp,DatosDeuda,Promedio,DeudaFbl5n,VariablesDeDesicion,TipoVariableValor,response,GarantiasPendienteInv,DeudaVencidaMayor,DatosPonderacionDso
from Models.ResponseGtia import Garantia , TipoCliente
from Dao.connectionDBMSQ import cnsDeuda,cnsCabecera, cnsMatrizGarantia, cnsfbl5n,cnsGarantiasEnt,cnsDecision,cnsGarantiasInv,cnsSiisa,cnsDeuda30D
import logging
import os
pathlog = os.environ.get('PATHLOG')
#pathlog = '/tmp/ecp.log'

logging.basicConfig(format='%(asctime)s %(message)s', datefmt='%Y/%m/%d %I:%M:%S %p', filename=pathlog, level=logging.INFO)



def Obtiene_Garantias_Enteradas(rut):
    try: 
        logging.info('%s Obtiene Garantias Enteradas : %s' , 'Ecp', rut)
        list =[]
        garantiasEnt = cnsGarantiasEnt(rut)
        for row in garantiasEnt:
            list.append(GarantiasEnterada(row[0],row[1],row[2],row[3],row[4],row[5]))    
        return list
    except Exception as e:
        logging.error('%s Error en la ejecución Obtiene garantias enteradas se retorna None: %s' , 'Ecp', e)
        return None

def Consulta_Ecp(rut):
    try:
        logging.info('%s Consulta General ECP : %s' , 'Ecp', rut)
        cabecera=Obtiene_Cabecera(rut)
        if (cabecera is not None):
            datos_deuda = Obtiene_Datos_Deuda(rut)
            desicion = Cns_Desicion(rut)
            garantias_enteradas = Obtiene_Garantias_Enteradas(rut)
            deudaFbl5n = Cns_DeudaFbl5n(rut)
            garantia_Inv = Cns_Garantia_Inv(rut)
            dso= Obtiene_dso(garantia_Inv,deudaFbl5n,datos_deuda,rut)
            for row in cabecera:
                respuesta=Ecp(row[0],row[1],row[2],row[3],Trx_Date(row[4]),row[7],Trx_Date(row[5]),Trx_Date(row[6]),dso[1],dso[2],
                datos_deuda,dso[0],desicion,garantias_enteradas,deudaFbl5n,garantia_Inv)
            return response('200','Consulta de riesgo exitosa',respuesta)
        else:
            return response('404','Sin datos consulta de riesgo',rut)
    except Exception as e:
        return response('500','Error Interno del Servidor',str(e))


def Obtiene_dso(garantia_Inv,deudaFbl5n,datos_deuda,rut):
    totalgp=0
    totalpr=0
    venta=0
    listdv =[]
    list5n = []
    listpr = []
    list = []
    listv = []
    listventa = []
    listsissa = []
    dso_pond= 0
    dso= 0
    try:
        logging.info('%s Obtiene DSO ' , 'Ecp')
        for row in garantia_Inv:
            totalgp = int(row.gtia_pendiente) + totalgp
        if totalgp > 0:
            listdv.append(DeudaVencidaMayor('SI','6%'))
            dso_pond = dso_pond + 6
        else:
            listdv.append(DeudaVencidaMayor('NO','-6%'))
            dso_pond = dso_pond - 6
            
            
            
        if (len(Cns_Deuda_30D(rut))>0):
            list5n.append(DeudaVencidaMayor('SI','6%'))
            dso_pond = dso_pond + 6
        else:  
            list5n.append(DeudaVencidaMayor('NO','-6%'))
            dso_pond = dso_pond - 6
        
        
        
        
        for row in datos_deuda:
            try:
                
                totalpr = row.q_protestos + totalpr
                
                #if isinstance(row.venta_relatorio, float):
                if (is_number(row.venta_relatorio)):
                    if (row.venta_relatorio > 0):
                        venta = venta +1
            except Exception:
                dso = row.promedio_dso  
                    
        if totalpr > 0:
            listpr.append(DeudaVencidaMayor('SI','6%'))
            dso_pond = dso_pond + 6
        else:
            listpr.append(DeudaVencidaMayor('NO','-6%')) 
            dso_pond = dso_pond -6
            
        
        if (venta>9):
            listventa.append(DeudaVencidaMayor('SI','-6%'))
            dso_pond = dso_pond - 6
        else:
            listventa.append(DeudaVencidaMayor('NO','6%')) 
            dso_pond = dso_pond + 6
            
            
        for row in cnsSiisa(rut):
            
            if (row[1]>0):
                dso_pond = dso_pond + 6
                deudaSissa = DeudaVencidaMayor(row[0],'6%')
            else:
                dso_pond = dso_pond - 6    
                deudaSissa = DeudaVencidaMayor(row[0],'-6')
            listsissa.append(deudaSissa)
            
        if (dso < 0):
            dso_pond = -1
        else:
            dso_pond =  round((dso_pond/100 * dso) + dso)        
        list.append(DatosPonderacionDso(round(dso),listsissa,listdv,list5n,listpr,listventa))
        list.append(dso_pond) 
        if (dso < 0):
            list.append('RECHAZADO')
        else:
            list.append(Obtiene_Riesgo(dso_pond))
        return list
    except Exception as e:
        logging.error('%s Error en la ejecución Obtiene DSO se retorna None: %s' , 'Ecp', e)
        return None

def Obtiene_Riesgo(dso_pond):
    try:
        if (dso_pond < 30):
            riesgo='BAJO'
        elif (dso_pond < 60):
            riesgo='MEDIO'
        elif (dso_pond < 90):
            riesgo='ALTO'
        else:
            riesgo='CRITICO'
        return riesgo
    except Exception as e:
        logging.error('%s Error en la ejecución Obtiene Riesgo se retorna None: %s' , 'Ecp', e)
        return None

def Obtiene_Cabecera(rut):
    try:
        logging.info('%s Obtiene Cabecera : %s' , 'Ecp', rut)
        cabecera=cnsCabecera(rut)
        if (len(cabecera)>0):
            return cabecera
        else: 
            return None
    except Exception as e:
        logging.error('%s Error en la ejecución Obtiene Cabecera se retorna None: %s' , 'Ecp', e)
        return None

def Obtiene_Datos_Deuda(rut):
    list = []
    sumaDeuda=0
    sumaFact=0
    sumaDso=0
    sumaQPago=0
    sumaVRelatorio=0
    sumaVenta=0
    largoRelatorios=0
    sumaQProtesto = 0
    largod=0
    largofac=0
    largodso=0
    largoqp=0
    largopro=0
    largov=0
    
    try: 
        logging.info('%s Obtiene Datos Deuda : %s' , 'Ecp', rut)
        deuda=Cns_Deuda(rut)
        for row in deuda:
            deudas=DatosDeuda(row[0],row[1],row[2],row[3],row[4],row[5],row[6],row[7])
            list.append(deudas)
            try:
                if (row[1] is not None):
                    sumaDeuda=sumaDeuda+row[1]
                    largod=largod +1
                    
                if (row[2] is not None):
                    sumaFact=sumaFact+row[2]
                    largofac=largofac+1
                if (row[3] is not None):
                    sumaDso=sumaDso+row[3]
                    largodso = largodso+1
                if (row[4] is not None):
                    sumaQPago=sumaQPago+row[4]
                    largoqp=largoqp +1
                if (row[5] is not None):
                    sumaQProtesto=sumaQProtesto+row[5]
                    largopro = largopro +1
                if not row[6]=='no informa':
                    if (row[6] is not None):
                            sumaVRelatorio=sumaVRelatorio+int(row[6])
                            largoRelatorios = largoRelatorios +1

                if (row[1] is not None):
                    sumaVenta=sumaVenta+row[7] 
                    largov=largov+1 
            except Exception as e:
                logging.error('%s Error en el calculo de promedios: %s' , 'Ecp', e)
                sumaDso=-1000
        
        
        if (largod ==0):
            sumaDeuda = 0
        else:
            sumaDeuda = round(sumaDeuda/largod)
            
        if (largofac ==0):
            sumaFact = 0
        else:
            sumaFact = round(sumaFact/largofac)
            
        if (largodso ==0):
            sumaDso = 0
        else:
            sumaDso = round(sumaDso/largodso)
            
        if (largoqp ==0):
            sumaQPago = 0
        else:
            sumaQPago = round(sumaQPago/largoqp)
            
        promedios = Promedio(sumaDeuda,sumaFact,sumaDso,sumaQPago,round(sumaQProtesto/largopro),round(sumaVRelatorio/largoRelatorios),round(sumaVenta/largov))
        list.append(promedios)
        return list
    except Exception as e:
        logging.error('%s Error en la ejecución Obtiene Datos Deuda se retorna None: %s' , 'Ecp', e)
        return None

def Cns_Deuda(rut):
    try:
        logging.info('%s Consulta Deuda : %s' , 'Ecp', rut)
        deuda=cnsDeuda(rut)
        return deuda
    except Exception as e:
        logging.error('%s Error en la ejecución Consulta  Deuda se retorna None: %s' , 'Ecp', e)
        return None
    
    
def Cns_Deuda_30D(rut):
    try:
        logging.info('%s Consulta Deuda  30D: %s' , 'Ecp', rut)
        deuda30D=cnsDeuda30D(rut)
        return deuda30D
    except Exception as e:
        logging.error('%s Error en la ejecución Consulta  Deuda 30D se retorna None: %s' , 'Ecp', e)
        return None

def Cns_DeudaFbl5n(rut):
    try:
        logging.info('%s Consulta DeudaFbl5n : %s' , 'Ecp', rut)
        list =[]
        deudaFbl5n = cnsfbl5n(rut)
        for row in deudaFbl5n:
            list.append(DeudaFbl5n(row[0],row[1],row[2],row[3],row[4],row[5],row[6]))    
        return list
    except Exception as e:
        logging.error('%s Error en la ejecución Consulta  DeudaFbl5n se retorna None: %s' , 'Ecp', e)
        return None

def Cns_Garantia_Inv(rut):
    try:
        list =[]
        logging.info('%s Consulta Garantia Inv: %s' , 'Ecp', rut)
        garantiaInv = cnsGarantiasInv(rut)
        for row in garantiaInv:
            list.append(GarantiasPendienteInv(row[0],row[1],row[2],row[3],row[4],row[5]))    
        return list
    except Exception as e:
        logging.error('%s Error en la ejecución Consulta Garantia Inv se retorna None: %s' , 'Ecp', e)
        return None

def Cns_Garantia():
    list =[]
    try:
        logging.info('%s Consulta Garantias ' , 'Ecp')
        garantia = cnsMatrizGarantia()
        for row in garantia:
            tipo=row[0]
            dso_ponderado=row[1]
            riesgo=row[2]
            local=row[3]
            modulo=row[4]
            tipoCliente=TipoCliente(tipo,dso_ponderado,riesgo,local,modulo)
            list.append(tipoCliente)    
        return response('200','Consulta de garantias exitosa ',Garantia(list))
    except Exception as e:
        logging.error('%s Error en la ejecución Consulta de Garantias se retorna None: %s' , 'Ecp', e)
        return response('500','Error Interno del Servidor',str(e))

def Cns_Desicion(rut):
    list =[]
    listf =[]
    listd = []
    listdp = []
    listvg = []
    listve =[]
    try:
        logging.info('%s Consulta Variables de desicion: %s' , 'Ecp', rut)
        decision = cnsDecision(rut)
        for row in decision:
            tipovar_fac = TipoVariableValor(row[0],row[1])
            tipovar_deu = TipoVariableValor(row[2],row[3])
            tipovar_deu_prom = TipoVariableValor(row[4],row[5])
            tipo_var_gar_ent = TipoVariableValor(row[6],row[7])
            tipo_var_exp = TipoVariableValor(row[8],row[9])
            listf.append(tipovar_fac)
            listd.append(tipovar_deu)
            listdp.append(tipovar_deu_prom)
            listvg.append(tipo_var_gar_ent)
            listve.append(tipo_var_exp)
            varDecision=VariablesDeDesicion(listf,listd,listdp,listvg,listve,row[10],row[11],row[12],row[13],row[14])
            list.append(varDecision)    
        return list
    except Exception as e:
        logging.error('%s Error en la ejecución Consulta Variables de desicion se retorna None: %s' , 'Ecp', e)
        return None

 
def Trx_Date(fecha):
    try:
        date_time_str = fecha
        date_time_obj = datetime.datetime.strptime(date_time_str, '%Y-%m-%d')
        return date_time_obj
    except Exception as e:
        logging.error('%s Error en la ejecución Transformacion de fechas Trx_Date  None: %s' , 'Ecp', e)
        return None
def is_number(s):
    try:
        float(s) # for int, long and float
    except ValueError:
        try:
            complex(s) # for complex
        except ValueError:
            return False

    return True