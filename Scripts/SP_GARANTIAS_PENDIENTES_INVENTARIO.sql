use ecp;
DROP PROCEDURE if exists SP_GARANTIAS_PENDIENTES_INVENTARIO;
DELIMITER $$
CREATE PROCEDURE SP_GARANTIAS_PENDIENTES_INVENTARIO(pRut varchar(10))
BEGIN
/*
se agrega tabla REISCN tiene el universo de los contratos vigentes
REISCN.estatus_actual filtro distinto "Finaliz."
se une con la z47 para determinar si el contrato esta vigente INNER JOIN
por defecto en la z47 agregar filtro VIGENTE = 'SI'
z47.cantidad filtramos los que son cero y unidad null

*/
DECLARE vPrecio numeric(10,2);
DROP TABLE IF EXISTS TMP_47;
CREATE TEMPORARY TABLE TMP_47 as
       SELECT z47.numero_contrato,z47.sociedad, z47.denominacion_contrato,z47.inicContr,
		SUM(DISTINCT cantidad) cantidad,
		 z47.UNIDAD
        FROM TBL_ZCLRE47 z47
	INNER JOIN TBL_REISCN r
		ON z47.numero_contrato = r.numero_contrato
            AND z47.sociedad 		= r.sociedad
       WHERE  (z47.unidad IS NOT NULL AND z47.cantidad>0)
	 AND COALESCE(z47.unidad,'%') NOT IN ('%')
	GROUP BY z47.numero_contrato,z47.sociedad,z47.denominacion_contrato,z47.inicContr;
 
 DROP TABLE IF EXISTS TMP_FBL1N;
CREATE TEMPORARY TABLE TMP_FBL1N AS
     SELECT f.numero_contrato,f.sociedad,f.cuenta ,f.Asignacion, f.moneda_del_documento, 
            f.moneda_local,SUM(f.Importe_en_moneda_doc) Importe_en_moneda_doc
      FROM TBL_FBL1N f
  GROUP BY f.numero_contrato,f.sociedad,f.cuenta ,f.Asignacion,f.moneda_del_documento, 
           f.moneda_local; 
        
  SELECT MAX(precio)
	INTO vPrecio
    FROM TBL_PRECIO_UF;

DROP TABLE IF EXISTS TMP_GARANTIAS;
    CREATE TEMPORARY TABLE TMP_GARANTIAS AS
     SELECT DISTINCT f.cuenta,z47.numero_contrato,COALESCE(b.fantasia, 
        z47.denominacion_contrato) fantasia,f.Asignacion,z47.inicContr, f.moneda_local, 
        z47.denominacion_contrato,f.moneda_del_documento,f.Importe_en_moneda_doc 
        Importe_en_moneda_doc_original,CASE WHEN f.moneda_del_documento = 'CLP' and 
        f.Importe_en_moneda_doc <6000 	THEN ROUND(f.Importe_en_moneda_doc/(select precio from TBL_PRECIO_UF where fecha =  concat(cast(now() as date),' 00:00:00')),2)
                when f.Importe_en_moneda_doc = 'UF' then f.Importe_en_moneda_doc  
                else round(f.Importe_en_moneda_doc ,2) 
			end Importe_en_moneda_doc,
			z47.UNIDAD,z47.Cantidad,/*f.N_documento,*/
             abs(FLOOR(z47.cantidad) + FLOOR(f.Importe_en_moneda_doc)) dif_cant_impor
	 FROM TMP_47 z47
LEFT JOIN TMP_FBL1N f 
		ON z47.numero_contrato = f.numero_Contrato
	   AND z47.Sociedad = f.sociedad
 LEFT JOIN TBL_BUC_BERTIN b
		ON f.cuenta = b.Rut_Cuenta_Deudor
     WHERE f.cuenta= pRut
       AND (unidad IS NOT NULL AND cantidad>0)
       and COALESCE(z47.unidad,'%') not in ('%');
    
        SELECT fantasia, asignacion, inicContr,
			CASE WHEN dif_cant_impor<1 THEN ABS(FLOOR(Importe_en_moneda_doc+0.49999)) /*38*/
							  ELSE ABS(FLOOR(cantidad+0.49999)) /*37*/
						 END GTIA_SOLICITADA,
            ABS(FLOOR(Importe_en_moneda_doc+0.49999)) GTIA_ENTRAGADA,/*siempre la 38*/
            ABS(FLOOR(cantidad+0.49999)) - ABS(FLOOR(Importe_en_moneda_doc+0.49999)) GTIA_PENDIENTE
		FROM TMP_GARANTIAS;
        
 END $$

DELIMITER ;
