
DROP PROCEDURE IF EXISTS  SP_VARIABLES_DECISION;
DELIMITER $$
create PROCEDURE SP_VARIABLES_DECISION(pRut varchar(10))

BEGIN
DECLARE vDeuda_total 				FLOAT; 
DECLARE vValor_UF 					FLOAT;
DECLARE vGarantiaActualEnterada_CLP NUMERIC;
DECLARE vGarantiaActualEnterada_UF  FLOAT;

				    SELECT SUM(importe)
							INTO vDeuda_total
							FROM TBL_BW
						 WHERE cliente = pRut; /*--'9492666';*/

						SELECT precio
						  INTO vValor_UF
							FROM TBL_PRECIO_UF
				 		 WHERE fecha = cast(substr(now(),1,10) AS DATETIME);

			 SELECT SUM(precio) 	precio_CLP, 
					SUM(cantidad)	Precio_UF
			  INTO vGarantiaActualEnterada_CLP,vGarantiaActualEnterada_UF
			  FROM (
					 SELECT DISTINCT 
							CASE WHEN z.unidad = 'UF' THEN round(z.cantidad*precio,1)
									   ELSE z.cantidad
								  END precio,
							z.cantidad 											--	
					  FROM TBL_ZCLRE47 z 
				INNER JOIN TBL_FBL1N f 
					    ON z.Sociedad = f.Sociedad
				INNER JOIN TBL_PRECIO_UF p
						ON p.fecha = cast(substr(now(),1,10) AS DATETIME)
					   AND z.numero_Contrato  =f.Numero_contrato
					 WHERE f.cuenta=pRut
					   AND (cantidad>0 AND z.unidad IS NOT NULL))sub;
                                          									
   			drop table IF EXISTS  tmp_sp_stock_deuda;
            drop table IF EXISTS   sub2;
			CREATE TEMPORARY TABLE tmp_sp_stock_deuda as
								SELECT m.fecha,
										SUM(sub3.stock_deuda) stock_deuda,
										sum(sub3.facturacion) facturacion
								FROM TBL_MESES_STOCK_DEUDA m
								LEFT JOIN	 (SELECT FIRST_DAY(fe_contabilizacion )  AS fecha ,
														SUM(CASE WHEN LENGTH(Cuenta)>4 AND t.cuenta_de_mayor IN (SELECT cc.valor FROM TBL_TABLA_CONVERSION cc WHERE cc.tipo= 'cuenta_de_mayor'and columna ='stock_deuda')
																					THEN importe_en_moneda_local 
																 ELSE 0 
															END) stock_deuda ,
														SUM(CASE WHEN LENGTH(Cuenta)>4 AND t.cuenta_de_mayor 	IN (SELECT cc.valor FROM TBL_TABLA_CONVERSION cc WHERE cc.tipo= 'cuenta_de_mayor' and columna = 'facturacion')
																					   AND t.clase_de_documento IN (SELECT cc.valor FROM TBL_TABLA_CONVERSION cc WHERE cc.tipo= 'clase_documento' and columna = 'facturacion')
																					   AND coalesce(t.indicador_cme,'') IN (SELECT coalesce(cc.valor,'') FROM TBL_TABLA_CONVERSION cc WHERE cc.tipo= 'indicador_cme' and columna = 'facturacion')
																					THEN importe_en_moneda_local 
																		 ELSE 0 
																END) facturacion,
														sum(importe_en_moneda_local) as Importe_en_moneda_local	,
														SUM(CASE WHEN t.clase_de_documento IN (SELECT cc.valor FROM TBL_TABLA_CONVERSION cc WHERE cc.tipo= 'clase_documento' and columna = 'q_plan_pago')
																THEN 1	ELSE 0
															   END) q_plan_pago,
														sum(case when coalesce(sub.clase_documento,'0')<>'0' then 1	else 0	end) q_protestos
												  FROM TBL_FBL5N t 
											LEFT JOIN (SELECT i.valor as indicador_cme,d.valor as clase_documento
														 FROM TBL_TABLA_CONVERSION i
												   CROSS JOIN TBL_TABLA_CONVERSION d
														WHERE i.columna = 'q_protestos'
														  AND i.tipo    = 'indicador_cme'
														  AND d.columna = 'q_protestos'
															  AND d.tipo    = 'clase_documento') sub
												ON t.clase_de_documento = sub.clase_documento
												and t.indicador_cme = sub.indicador_cme
												WHERE cuenta = pRut
												GROUP BY sociedad, FIRST_DAY(fe_contabilizacion) ,	cuenta   ) sub3	
											on m.fecha = LAST_DAY(sub3.FECHA)
										GROUP BY m.fecha,sub3.q_plan_pago,sub3.q_protestos 
										ORDER BY m.fecha asc  ; 
   
   CREATE TEMPORARY TABLE sub2 as 
                   SELECT * 
					 FROM tmp_sp_stock_deuda;
                     		                   
                 
		  SELECT    ROUND(AVG(Facturacion  ),0) 	 										Facturacionpromedio12M_clp,
				    ROUND(AVG(Facturacion)/vValor_UF,2) 						Facturacionpromedio12M_UF	,
				    vDeuda_total 												Deuda_Actual_clp,
				    ROUND((vDeuda_total/vValor_UF),2) 							Deuda_Actual_uf,
					round(avg(Stock_deuda),0) 									DeudaPromedio12M_CLP,
					round(avg(Stock_deuda)/vValor_UF,2) 						DeudaPromedio12M_UF,
					vGarantiaActualEnterada_CLP									GarantiaActualEnterada_CLP,
					vGarantiaActualEnterada_UF									GarantiaActualEnterada_UF,
					(vDeuda_total-vGarantiaActualEnterada_CLP)							Exposicion_deudaGarantia_CLP,
					round(((vDeuda_total-vGarantiaActualEnterada_CLP)/vValor_UF),2) 	Exposicion_deudaGarantia_UF,                    
                    CASE WHEN (vGarantiaActualEnterada_CLP/round(avg(Facturacion),0) )<10 
							 THEN CONCAT(ROUND((vGarantiaActualEnterada_CLP/round(avg(Facturacion),0) ),1), " GAT")
						 ELSE 'REVISAR UNIDAD GARANTIAS'
					END GarantiasPromedio,
                    COALESCE(CONCAT(ROUND((vDeuda_total-vGarantiaActualEnterada_CLP)/vDeuda_total,1)," GAT"),'SIN DEUDA ACTUAL') CoberturaActual,
                     case when ROUND((vDeuda_total-vGarantiaActualEnterada_CLP)/vGarantiaActualEnterada_CLP,1)<20
						   then  concat(ROUND((vDeuda_total-vGarantiaActualEnterada_CLP)/vGarantiaActualEnterada_CLP,1), " GAT")
                           else "REVISAR UNIDAD GARANTIAS"
					end CoberturaPromedio,
                    COALESCE(concat(ROUND(vDeuda_total/ROUND(AVG(Facturacion  ),0),1), " VECES"), "SIN FACTURACION EN 12M") DeudaVecesFacturacion,
                    CASE WHEN (vDeuda_total-vGarantiaActualEnterada_CLP)/round(AVG(Facturacion  ),0)<0 THEN "0%"
						 ELSE concat(round((vDeuda_total-vGarantiaActualEnterada_CLP)/round(AVG(Facturacion  ),0)*100,0),"%")
					END ExposicionPorcentual							

                    FROM ( SELECT sub1.fecha FECHA,
								 COALESCE(stock_deuda,0) stock_deuda,
								 COALESCE(sub1.facturacion,0) facturacion 
							FROM (SELECT s.fecha,
										(SELECT SUM( ss.stock_deuda) 
											FROM sub2 ss
											WHERE ss.fecha <=s.fecha) AS stock_deuda,
										s.facturacion		
									FROM tmp_sp_stock_deuda s) sub1
							WHERE sub1.fecha >=LAST_DAY(DATE_SUB(DATE_FORMAT(NOW(), '%Y-%m-%d'), INTERVAL 12 month))) totales ;
                   
   			drop table IF EXISTS  tmp_sp_stock_deuda;
            drop table IF EXISTS   sub2;
     END $$

DELIMITER ;
