USE ecp_db;
DROP procedure if exists SP_STOCK_DEUDA;
DELIMITER $$

CREATE procedure SP_STOCK_DEUDA(pRut varchar(10))

BEGIN
DECLARE FECHA_ACTUAL DATE;
DECLARE MAX_FECHA DATE;
SELECT LAST_DAY(DATE_SUB(DATE_FORMAT(NOW(), '%Y-%m-%d'), INTERVAL 1 month))
				INTO FECHA_ACTUAL;
SELECT MAX(FECHA) 
INTO MAX_FECHA
FROM TBL_MESES_STOCK_DEUDA;
			  
IF MAX_FECHA < FECHA_ACTUAL THEN 
INSERT INTO TBL_MESES_STOCK_DEUDA VALUES (FECHA_ACTUAL);
END IF;

DROP TABLE IF EXISTS tmp_stock_deuda_acum;
DROP TABLE IF EXISTS tmp_stock_deuda;

CREATE TEMPORARY TABLE tmp_stock_deuda (
fecha datetime,
stock_deuda numeric,
facturacion numeric,
q_plan_pago numeric,
q_protestos numeric);

CREATE TEMPORARY TABLE tmp_stock_deuda_acum (
fecha datetime,
stock_deuda numeric,
facturacion numeric,
q_plan_pago numeric,
q_protestos numeric);

insert into tmp_stock_deuda
	SELECT m.fecha,
			SUM(sub3.stock_deuda) stock_deuda,
			sum(sub3.facturacion) facturacion,
			sub3.q_plan_pago,
			sub3.q_protestos
	FROM TBL_MESES_STOCK_DEUDA m
	LEFT JOIN	 (SELECT FIRST_DAY(fe_contabilizacion )  AS fecha ,
							SUM(CASE WHEN LENGTH(Cuenta)>4 AND t.cuenta_de_mayor IN (SELECT cc.valor FROM TBL_TABLA_CONVERSION cc WHERE cc.tipo= 'cuenta_de_mayor'and columna ='stock_deuda')
														THEN importe_en_moneda_local 
									 ELSE 0 
								END) stock_deuda ,
							SUM(CASE WHEN LENGTH(Cuenta)>4 AND t.cuenta_de_mayor 	IN (SELECT cc.valor FROM TBL_TABLA_CONVERSION cc WHERE cc.tipo= 'cuenta_de_mayor' and columna = 'facturacion')
														   AND t.clase_de_documento IN (SELECT cc.valor FROM TBL_TABLA_CONVERSION cc WHERE cc.tipo= 'clase_documento' and columna = 'facturacion')
														   AND coalesce(t.indicador_cme,'') IN (SELECT coalesce(cc.valor,'') FROM TBL_TABLA_CONVERSION cc WHERE cc.tipo= 'indicador_cme' and columna = 'facturacion')
														THEN importe_en_moneda_local 
											 ELSE 0 
									END) facturacion,
							sum(importe_en_moneda_local) as Importe_en_moneda_local	,
							SUM(CASE WHEN t.clase_de_documento IN (SELECT cc.valor FROM TBL_TABLA_CONVERSION cc WHERE cc.tipo= 'clase_documento' and columna = 'q_plan_pago')
									THEN 1	ELSE 0
								   END) q_plan_pago,
							sum(case when coalesce(sub.clase_documento,'0')<>'0' then 1	else 0	end) q_protestos
					  FROM TBL_FBL5N t 
				LEFT JOIN (SELECT i.valor as indicador_cme,d.valor as clase_documento
							 FROM TBL_TABLA_CONVERSION i
					   CROSS JOIN TBL_TABLA_CONVERSION d
							WHERE i.columna = 'q_protestos'
							  AND i.tipo    = 'indicador_cme'
							  AND d.columna = 'q_protestos'
								  AND d.tipo    = 'clase_documento') sub
					ON t.clase_de_documento = sub.clase_documento
					and t.indicador_cme = sub.indicador_cme
					WHERE cuenta = pRut
					GROUP BY sociedad, FIRST_DAY(fe_contabilizacion) ,	cuenta   ) sub3	
				on m.fecha = LAST_DAY(sub3.FECHA)
			GROUP BY m.fecha,sub3.q_plan_pago,sub3.q_protestos 
			ORDER BY fecha desc  ;                                                                  
insert into tmp_stock_deuda_acum 
select * from tmp_stock_deuda;

	SELECT DISTINCT sub1.fecha FECHA,
			 coalesce(stock_deuda,0) stock_deuda,
			 COALESCE(sub1.facturacion,0) facturacion  ,
			 ROUND(CASE WHEN stock_deuda = 0 AND facturacion > 0 THEN 0
						WHEN stock_deuda < 0 AND facturacion > 0 THEN 0 
						WHEN stock_deuda < 0 AND facturacion = 0 THEN NULL
						WHEN (stock_deuda/facturacion*30)>0	THEN (stock_deuda/facturacion*30)
						
						/*eLSE 0*/
							END,0)  DSO,
		   coalesce(sub1.q_plan_pago,0) q_plan_pago,
			coalesce(sub1.q_protestos,0) q_protestos,
			coalesce(ROUND((r.venta*1.19),0),0) venta,
			coalesce(CASE WHEN venta = 0 THEN 0
				  ELSE ROUND(((facturacion/ROUND((r.venta*1.19),0)))*100,0) 
		 end ,0) CostoVenta
		FROM (
			 SELECT s.fecha,
							(select SUM( ss.stock_deuda) 
								from tmp_stock_deuda_acum ss
								where ss.fecha <=s.fecha) AS stock_deuda,
							s.facturacion,
							s.q_plan_pago,
							s.q_protestos		
				FROM tmp_stock_deuda s) sub1
				left JOIN TBL_RELATORIOS_BW r
					ON sub1.fecha = r.fecha
					 AND r.cliente = pRut
				WHERE sub1.fecha >=LAST_DAY(DATE_SUB(DATE_FORMAT(NOW(), '%Y-%m-%d'), INTERVAL 12 month))
				order by 1 asc;

drop table  tmp_stock_deuda_acum;
drop table  tmp_stock_deuda;
END $$

DELIMITER ;
