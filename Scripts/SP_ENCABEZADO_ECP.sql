use ecp_db;

drop procedure if exists SP_ENCABEZADO_ECP;
DELIMITER $$

CREATE procedure SP_ENCABEZADO_ECP(pRut varchar(10))

BEGIN
		
				SELECT DISTINCT 
								rut_cuenta_deudor,
								razon_social,
								fantasia,
								Rubro_CC_Especifico rubro_cc,
								 DATE_FORMAT(NOW(), '%Y-%m-%d') AS "Fecha_base_ECP",
								 DATE_FORMAT(NOW(), '%Y-%m-%d') AS "Fecha_Emision_ECP",
								 DATE_FORMAT(NOW() + INTERVAL 90 DAY , '%Y-%m-%d') AS "Fecha_caducidad_ECP",
								round((SELECT precio
									FROM TBL_PRECIO_UF
									WHERE fecha =  DATE_FORMAT(NOW(), '%Y-%m-%d')),2) precio
				 FROM TBL_BUC_BERTIN
				WHERE rut_cuenta_deudor = PRUT
                order by Fecha_Actualizacion desc
				LIMIT 1;

END $$

DELIMITER ;
